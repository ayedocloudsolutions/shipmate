/*
Copyright © 2021  BJOERN MATHIS

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

import (
	"fmt"
	"os"
)

// not using a git api for go for now as they do not provide all features, falling back to syscalls
// builds a semantic versioning string which is returned by this function
func BuildVersion(path string) string {
	fileInfo, err := os.Stat(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Given path (%s) does not exist.\n", path)
		os.Exit(1)
	}

	if fileInfo.IsDir() {
		os.Chdir(path)
	} else {
		fmt.Fprintf(os.Stderr, "Given path (%s) is not a directory.\n", path)
		os.Exit(1)
	}
	if !IsGitRepo() {
		fmt.Fprintf(os.Stderr, "Not a git repo!\n")
		os.Exit(1)
	}
	branch := GitBranch()
	if !isValidString(branch) {
		fmt.Fprintf(os.Stderr, "The current branch name contains illegal characters (not in [.-a-zA-Z0-9]): %s\n", branch)
		os.Exit(1)
	}
	id := GitCommitId()
	tag := GitTag()
	return fmt.Sprintf("%s.rc-%s-%s", tag, branch, id)
}
