package utils

import (
	"fmt"
	"os"
)

func GetCwd() string {
	cwd, err := os.Getwd()
	CheckErr(err)
	return cwd
}

func InitPath(path string) {
	// Check if path exists
	_, err := os.Stat(path)
	CheckErr(err)

	// Check if path is a git repository
	if !IsGitRepo() {
		fmt.Println("Not a git repository")
		os.Exit(1)
	}
}
