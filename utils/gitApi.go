/*
Copyright © 2021  BJOERN MATHIS

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	git "github.com/go-git/go-git/v5"
	plumbing "github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	commit "github.com/wfscheper/convcom"
)

func getGitRepository(path string) (*git.Repository, error) {
	r, err := git.PlainOpen(path)
	CheckErr(err)

	return r, err
}

// func getCommitList(r *git.Repository, start string, end string) ([]*object.Commit, error) {
// 	commitIter, err := r.Log(
// 		&git.LogOptions{
// 			From:  start,
// 			Since: end,
// 		},
// 	)
// 	return commitIter, err

// }

// checks if the current directory is a git repo by running git status
func IsGitRepo() bool {
	_, err := exec.Command("git", "status").Output()

	return err == nil
}

// tries to get the current git tag, if it fails 0.0.1 is returned
func GitTag() string {
	tags := getGitTagList()
	tag := tags[len(tags)-1]
	return tag.Name().Short()
}

func getGitTagList() []*plumbing.Reference {
	r, err := getGitRepository(".")
	CheckErr(err)

	tagrefs, err := r.Tags()
	CheckErr(err)

	taglist := []*plumbing.Reference{}

	err = tagrefs.ForEach(func(t *plumbing.Reference) error {
		taglist = append(taglist, t)
		return nil
	},
	)
	CheckErr(err)
	return taglist
}

func getGitCommitList() []*object.Commit {
	r, err := getGitRepository(".")
	CheckErr(err)

	// if start == "" {
	// 	// get current branch
	// 	ref, err := r.Head()
	// 	CheckErr(err)

	// 	// set start to commit hash of HEAD
	// 	start = ref.Hash().String()
	// }

	// get list of tags
	taglist := getGitTagList()
	// set end to the commit hash of the latest git tag
	tag := taglist[len(taglist)-1]
	from := tag.Hash()

	commitIter, err := r.Log(&git.LogOptions{From: from})
	CheckErr(err)

	commitlist := []*object.Commit{}

	err = commitIter.ForEach(func(c *object.Commit) error {
		commitlist = append(commitlist, c)
		return nil
	})
	CheckErr(err)

	return commitlist
}

func isConventionalCommit(c *object.Commit) (*commit.Commit, error) {
	cfg := &commit.Config{}
	p, _ := commit.New(cfg)

	cc, err := p.Parse(c.Message)
	if err != nil {
		return cc, err
	}

	return cc, err

}

func GetConventionalCommits() {
	commitlist := getGitCommitList()

	cclist := []map[string]string{}

	for _, commit := range commitlist {
		
		
		cc, err := isConventionalCommit(commit)
		CheckErr(err)

		if cc != nil {
			sc := map[string]string{
				"hash": commit.Hash.String(),
				"id": commit.Hash()

			}
		}
	}

}

func GitTags() {
	tags := getGitTagList()

	for _, tag := range tags {
		fmt.Println(tag.Name())
	}
}

// tries to get the current git branch, exists the program on failure
func GitBranch() string {
	r, err := getGitRepository(".")
	CheckErr(err)

	ref, err := r.Head()
	CheckErr(err)

	return ref.Name().Short()
	// out, err := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD").Output()

	// if err != nil {
	// 	fmt.Fprintf(os.Stderr, "Error while trying to get branch: %s\n", err)
	// 	os.Exit(1)
	// }
	// return strings.TrimSpace(string(out))
}

// tries to get the current git repository, exists the program on failure
func GitRepository() string {
	// Derive repository from GitLab
	if repository, repository_found := os.LookupEnv("CI_REPOSITORY_URL"); repository_found {
		return strings.TrimSpace(repository)
	}

	// Derive repository from shell
	out, err := exec.Command("git", "config", "--get", "remote.origin.url").Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while trying to get branch: %s\n", err)
		os.Exit(1)
	}
	return strings.TrimSpace(string(out))
}

// tries to get the current git has in short form, exists the program on failure
func GitCommitHash() string {
	r, err := getGitRepository(".")
	CheckErr(err)

	ref, err := r.Head()
	CheckErr(err)

	commit, err := r.CommitObject(ref.Hash())
	CheckErr(err)
	fmt.Println(commit)

	return ref.Hash().String()

	// out, err := exec.Command("git", "rev-parse", "HEAD").Output()

	// if err != nil {
	// 	fmt.Fprintf(os.Stderr, "Error while trying to get hash: %s\n", err)
	// 	os.Exit(1)
	// }
	// return strings.TrimSpace(string(out))
}

func GitCommits(start string, end string) []string {
	// out, err := exec.Command("git", "rev-list", strings.Join([]string{start, end}, "..")).Output()

	// if err != nil {
	// 	fmt.Fprintf(os.Stderr, "Error while trying to get hash: %s\n", err)
	// 	os.Exit(1)
	// }
	return []string{""}
}

// tries to get the current git has in short form, exists the program on failure
func GitCommitId() string {
	out, err := exec.Command("git", "rev-parse", "--short", "HEAD").Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while trying to get id: %s\n", err)
		os.Exit(1)
	}
	return strings.TrimSpace(string(out))
}
