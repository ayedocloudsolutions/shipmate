## [1.1.0](https://gitlab.com/ayedocloudsolutions/shipmate/compare/v1.0.0...v1.1.0) (2021-08-31)


### :bug: Fixes

* refactored functions ([ead5589](https://gitlab.com/ayedocloudsolutions/shipmate/commit/ead558922d0d1ee14aaf9c17941772b9aa8ad285))
* refactored functions ([1d73a22](https://gitlab.com/ayedocloudsolutions/shipmate/commit/1d73a2254dc2468fcea403b821d61268c5ed5a39))
* refactored functions ([fa1a9ad](https://gitlab.com/ayedocloudsolutions/shipmate/commit/fa1a9ad6d91b3458167b96b0a295c17487ea4d17))
* refactored functions ([93c0ac6](https://gitlab.com/ayedocloudsolutions/shipmate/commit/93c0ac6c15d748b1a28411a144ea41e016ce5c93))
* refactored functions ([badf981](https://gitlab.com/ayedocloudsolutions/shipmate/commit/badf981b83ed7d4674a7f771b7b0cde436e37554))
* refactored functions ([cc39ed4](https://gitlab.com/ayedocloudsolutions/shipmate/commit/cc39ed4cf2e47bb9410793a9cd5cabd8fc2bdb4c))
* refactored functions ([0bafac1](https://gitlab.com/ayedocloudsolutions/shipmate/commit/0bafac14db7db6d5123f8d34a9fa1e44471fd9ae))
* refactored functions ([78b9ee4](https://gitlab.com/ayedocloudsolutions/shipmate/commit/78b9ee4bf95fa6f49045c3856493533a75016e93))
* refactored functions ([b387e3b](https://gitlab.com/ayedocloudsolutions/shipmate/commit/b387e3ba98678f4f3274f7e3fa3687d84e2c7623))
* refactored functions ([aadd582](https://gitlab.com/ayedocloudsolutions/shipmate/commit/aadd582ae6e44803ed251e62eec1fa15139bb799))
* refactored functions ([e14e335](https://gitlab.com/ayedocloudsolutions/shipmate/commit/e14e3354df4cd2a81fd7e3be31463a57d8f40085))
* refactored functions ([eb999b6](https://gitlab.com/ayedocloudsolutions/shipmate/commit/eb999b655a38ebb0303863186162021fc0d14b2b))
* refactored functions ([caa5224](https://gitlab.com/ayedocloudsolutions/shipmate/commit/caa5224261ed7a3726a6b31d2e1bd65d1e53f458))
* stuff ([88ec207](https://gitlab.com/ayedocloudsolutions/shipmate/commit/88ec207efed79e7e14390c4cbe67c2d98013d247))


### :sparkles: Features

* added logo ([ebf2c71](https://gitlab.com/ayedocloudsolutions/shipmate/commit/ebf2c7105101c22a7268883f426386434212c6c8))
* added logo ([37ad974](https://gitlab.com/ayedocloudsolutions/shipmate/commit/37ad97460b6a9aa7a812126fae632866c40d6d7d))

## 1.0.0 (2021-08-31)


### :memo: Documentation

* added requirements ([ad3ce23](https://gitlab.com/ayedocloudsolutions/shipmate/commit/ad3ce239e70690690b18ddf7893256ac7f299235))


### :sparkles: Features

* added Dockerfile ([e4d1c9c](https://gitlab.com/ayedocloudsolutions/shipmate/commit/e4d1c9c052a8d26f3c405362b18bac84d44e45af))
* added Dockerfile ([ec6187d](https://gitlab.com/ayedocloudsolutions/shipmate/commit/ec6187d1ec0a1c7887b7bef30e586de81de0d019))
* update shipmate.sh ([bea28fe](https://gitlab.com/ayedocloudsolutions/shipmate/commit/bea28fed6f1d69eb9197e670530c49e951dbba9a))
* update shipmate.sh ([11f24e5](https://gitlab.com/ayedocloudsolutions/shipmate/commit/11f24e52975bfcec91892174d5b8bff19d497e3a))


### :bug: Fixes

* stuff ([d280a36](https://gitlab.com/ayedocloudsolutions/shipmate/commit/d280a36208c07dfdf5a6fd8cd276370b92482fe0))
