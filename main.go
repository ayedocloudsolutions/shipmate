/*
Copyright © 2021 Fabian Peter <fp@ayedo.de>
This file is part of CLI application foo.
*/
package main

import "shipmate/cmd"

func main() {
	cmd.Execute()
}
