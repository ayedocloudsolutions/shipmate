#!/bin/bash
#
# Author:   Bruno Gallace - Ayedo GmbH
# Function: This Skript change the tag version in an Helm Chart values.yaml file.

#
# Defaults 
#
# Set Defaults
DEBUGLEVEL=0
METHOD="CLI"
OUTPUTFILE="stable_image_values.yaml"
CSVFILE="stable_image_version.csv"

#
# Set Colors
#
_FONT_RED=`tput setaf 1`
_FONT_YELLOW=`tput setaf 3`
_FONT_GREEN=`tput setaf 2`
_RESET=`tput sgr0`

#
# Colorize Font WARN, ERR, INFO
#
_INFO="${_FONT_GREEN}INFO:${_RESET}\t"
_WARN="${_FONT_YELLOW}WARN:${_RESET}\t"
_ERR="${_FONT_RED}ERR:${_RESET}\t"

#
# Get Options -m SELF ...
#
while getopts m:i:t:f:c:o:d: flag
do
    case "${flag}" in
        m) METHOD=${OPTARG};;
        i) IMAGENAME=${OPTARG};;
        t) NEWTAG=${OPTARG};;
        f) INPUTFILE=${OPTARG};;
        c) CSVFILE=${OPTARG};;
        o) OUTPUTFILE=${OPTARG};;
        d) DEBUGLEVEL=${OPTARG};;
        *) echo "${_ERR} unsupported flag detected. Ignoring it.";;
    esac
done

#
# Prints debug messages
#
function debug_output() {
    LEVEL=$1
    MESSAGE=$2
    if [[ $LEVEL -eq $DEBUGLEVEL || $LEVEL -lt $DEBUGLEVEL ]]
    then
        echo $MESSAGE
    fi
}

#
# Check the tags and tell me if everething is ok.
#
function check_tag() {
    NEWTAG=$1
    HIT=false
    # CHECK: praefix v1.0.1
    if [[ $NEWTAG =~ ^[v]{1} ]]
    then
        HIT=true
        debug_output 0  "${_WARN} v praefix detected: $NEWTAG"
    fi
    # CHECK: latest or stable
    if [[ $NEWTAG =~ ^[a-zA-Z]{4,} ]]
    then
        HIT=true
        debug_output 0 "${_WARN} latest, stable or next detected: $NEWTAG"
    fi
    # CHECK: Versions like 1.0.0 and 1.0.2-mybranch-name-build-f4e56a8
    if [[ $NEWTAG =~ ^[0-9]{1,}[.]{1}[0-9]{1,}[.]{1}[0-9]{1,}[a-z0-9-]{0,}$ ]]
    then
        HIT=true
        debug_output 1 "${_INFO} Given tag is ok: $NEWTAG"
    fi
    # If no CHECK has an HIT, print this
    if ! $HIT
    then
        debug_output 0 "${_WARN} Is this tag ok? $NEWTAG"
    fi
}

#
# Check the imagename. If something is wrong exit.
#
function check_imagename() {
    IMAGENAME=$1
    if [[ $IMAGENAME =~ ^[a-z0-9/-]{1,}$ ]]
    then
        debug_output 1 "${_INFO} Imagename ok: $IMAGENAME"
    else
        debug_output 0 "${_ERR} Your imagename is not ok. Allowed characters [a-z] [0-9] [-] and [/]: $IMAGENAME"
        set -e
        exit 1
    fi
}

#
# Checks if the file exists.
#
function check_file_exist() {
    FILE=$1
    if [[ -f $FILE ]]
    then
        debug_output 1 "${_INFO} File exist $FILE"
    else
        debug_output 0 "${_ERR} Can not find file: $FILE"
        set -e
        exit 1
    fi
}

#
# Open values.yaml, change the tag version and validate the change.
#
function refresh_values() {
    IMAGENAME=$1
    NEWTAG=$2
    INPUTFILE=$3
    OUTPUTFILE=$4
    COUNT=0
    # Some checks
    check_tag $NEWTAG
    check_imagename $IMAGENAME
    check_file_exist $INPUTFILE

    # Check if the value needs an update:
    COUNT=$(grep "$IMAGENAME:$NEWTAG" $INPUTFILE | wc -l)
    if [[ $COUNT -gt 0 ]]
    then
        debug_output 0 "${_INFO} No change needed for ${IMAGENAME}:${NEWTAG}"
    else
        # Check imagename for slashes replace / with \/
        IMAGENAMESLASH=$(echo $IMAGENAME | sed -E "s/\//\\\\\//g")
        # Replace the Tag
        sed -E "s/(.*image:.*\/${IMAGENAMESLASH}:).*$/\1${NEWTAG}/g" ${INPUTFILE} > tmp
        # When sed writes to the sourcefile, the file ist empty. So, create a tmp file and write it back
        mv tmp $OUTPUTFILE

        # Check if the replacement was succesfully. Count all changes.
        COUNT=0
        COUNT=$(grep "$IMAGENAME:$NEWTAG" $OUTPUTFILE | wc -l)
        
        debug_output 3 "${_INFO} Input: $INPUTFILE Output: $OUTPUTFILE"

        if [[ $COUNT -eq 1 ]]
        then
            debug_output 0 "${_INFO} $COUNT Tag changed to ${IMAGENAME}:${NEWTAG}"
        elif [[ $COUNT -gt 1 ]]
        then
            debug_output 0 "${_WARN}${_FONT_RED}$COUNT Tags${_RESET} changed to ${IMAGENAME}:${NEWTAG}"
        else
            debug_output 0 "${_ERR} Replacement failed. Please Check \"$IMAGENAME\" for typos."
            set -e
            exit 1
        fi
    fi
}

function __main__() {
    if [[ $METHOD = "CLI" && ! -z $IMAGENAME && ! -z $NEWTAG && ! -z $INPUTFILE ]]
    then
        debug_output 2 "${_INFO} ARGS detected"
        if [[ $OUTPUTFILE = $INPUTFILE ]]
        then
            debug_output 0 "${_ERR} Input and Output are identical. Aborting!"
            set -e
            exit 1
        fi
        refresh_values $IMAGENAME $NEWTAG $INPUTFILE $OUTPUTFILE
    elif [[ $METHOD = "CSV" && ! -z "$INPUTFILE" ]]
    then
        # Check if csv file exist.
        check_file_exist $CSVFILE
        debug_output 3 "${_INFO} Input: $INPUTFILE Output: $OUTPUTFILE"

        if [[ $OUTPUTFILE = $INPUTFILE ]]
        then
            debug_output 0 "${_ERR} Input and Output are identical. Aborting!"
            set -e
            exit 1
        else
            rm $OUTPUTFILE > /dev/null
            # We need to copy the file and use this for multiple changes
            cp $INPUTFILE $OUTPUTFILE
        fi

        debug_output 2 "${_INFO} cp $INPUTFILE $OUTPUTFILE"
        exec < $CSVFILE
        read header
        while IFS="," read -r imagename new_tag
        do
            refresh_values $imagename $new_tag $OUTPUTFILE $OUTPUTFILE
        done
    else
        echo ""
        echo "${_ERR} Missing Arguments"
        echo ""
        echo "If Method is CLI (default)"
        echo "${_FONT_GREEN}sh $0 -i \"mssql/server\" -t \"2019-CU12-ubuntu-20.04\" -f \"path/to/value.yaml\"${_RESET}"
        echo "${_FONT_GREEN}sh $0 -m CLI -i \"your-imagename\" -t \"1.2.12\" -f \"path/to/value.yaml\" -o values_2.yaml -d 3${_RESET}"
        echo ""
        echo "If Method is CSV:"
        echo "${_FONT_YELLOW}sh $0 -m CSV -f path/to/value.yaml${_RESET}"
        echo "${_FONT_YELLOW}sh $0 -m CSV -f path/to/value.yaml -c stable_images.csv -o new_values.yaml -d 1${_RESET}"
        echo ""
        echo "Example CSV:"
        echo "imagename,new_tag"
        echo "your-authenticationservice,2.3.1"
        echo "your-authorizationservice,2.3.0"
        echo ""
        echo "Params:"
        echo "\t -m [STRING] \t REQUIRED: CSV or CLI"
        echo "\t -f [STRING] \t REQUIRED: Path to Sourcefile."
        echo "\t -i [STRING] \t REQUIRED: for CLI: Searchstring Imagename."
        echo "\t -t [STRING] \t REQUIRED: for CLI: Replace with New Tag"
        echo "\t -c [STRING] \t OPTIONAL: for CSV: Path to CSV file. Default ist stable_image_version.csv"
        echo "\t -o [STRING] \t OPTIONAL: Output file. Default ist stable_image_values.yaml"
        echo "\t -d [NUMBER] \t OPTIONAL: Debuglevel. Default is 0"
        echo "\t\t\t Available Debuglevels: 0-2"
        echo "\t\t\t Disable Output: \"-1\""
        echo ""
    fi
}

__main__