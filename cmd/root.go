/*
Copyright © 2021 Fabian Peter <fp@ayedo.de>
This file is part of CLI application foo.
*/
package cmd

import (
	"fmt"
	"os"

	"shipmate/utils"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string
var path string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "shipmate",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.shipmate.yaml)")

	rootCmd.PersistentFlags().StringVar(&path, "path", utils.GetCwd(), "Working directory")
	viper.BindPFlag("path", rootCmd.PersistentFlags().Lookup("path"))

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	// - open path
	// - check if git repo
	// 	- exit if not
	utils.InitPath(path)

	// - get branch
	viper.SetDefault("git.branch", utils.GitBranch())
	// - get commit
	viper.SetDefault("git.commit.hash", utils.GitCommitHash())
	viper.SetDefault("git.commit.id", utils.GitCommitId())
	// - get last tag
	viper.SetDefault("git.tag", utils.GitTag())
	// - get last version
	viper.SetDefault("version.last", utils.GitTag())
	// - get commits since last tag
	viper.SetDefault("git.commits", "")
	// - analyze commits
	// - weed out non conventional commits
	// - compute next version
	// - set release candidate version
	// - compute changelog

	utils.GetConventionalCommits()

	// Get git info
	viper.SetDefault("git.repository", utils.GitRepository())

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".shipmate" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".shipmate")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

	fmt.Println("git.commit.hash", viper.GetString("git.commit.hash"))
	fmt.Println("git.commit.id", viper.GetString("git.commit.id"))
	fmt.Println("git.repository", viper.GetString("git.repository"))
	fmt.Println("git.branch", viper.GetString("git.branch"))
}
