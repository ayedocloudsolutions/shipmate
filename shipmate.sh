#!/bin/bash
set -e

init-shipment() {
  export DEFAULT_VERSION="0.0.1"
  
  export CI_BRANCH=$(echo $CI_COMMIT_REF_NAME | sed 's/refs\/heads\///g' | tr _/ -)

  # Check if current directory is a git repo
  # git rev-parse --is-inside-work-tree
  # https://stackoverflow.com/questions/2180270/check-if-current-directory-is-a-git-repository
  IS_GIT_REPO="$(git rev-parse --is-inside-work-tree 2>/dev/null)"
  if [ "$IS_GIT_REPO" = "true" ]; then
    export BUILD_BRANCH=${CI_BRANCH:-$(git rev-parse --abbrev-ref HEAD | tr _/ -)}
    export BUILD_COMMIT=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}
    export _LATEST_TAG=$(git describe --abbrev=0 --tags | cut -d'-' -f1 | sed 's/v//g')
    export LATEST_TAG=${_LATEST_TAG:-$DEFAULT_VERSION}
  else
    echo "NO GIT REPO"
    export BUILD_BRANCH=${CI_BRANCH:-"main"}
    export BUILD_COMMIT=${CI_COMMIT_SHORT_SHA:-"42af941"}
    export LATEST_TAG=$DEFAULT_VERSION
  fi
  export BUILD_VERSION_SUFFIX="rc"

  export LATEST_VERSION=${LATEST_VERSION:-$LATEST_TAG}

  # Check if RELEASE_VERSION is empty
  if [ -z "$RELEASE_VERSION" ]; then
    # Check if file RELEASE_VERSION (see release-code) exists
    if [ -f RELEASE_VERSION ];
    then
      export RELEASE_VERSION=$(cat RELEASE_VERSION)
    fi
  fi

  # Check if NEXT_VERSION is empty
  if [ -z "$NEXT_VERSION" ]; then
    # Check if file RELEASE_VERSION (see generate-next-version) exists
    if [ -f NEXT_VERSION ]; then
      export NEXT_VERSION=$(cat NEXT_VERSION)
    else
      # If no NEXT_VERSION is found (typically happens with "chore:" type commits), set to LATEST_VERSION
      export NEXT_VERSION=${LATEST_VERSION}
    fi
  fi

  # 1.0.1-rc.feature-fix-fail-abdefg2
  export _BUILD_VERSION="$NEXT_VERSION-$BUILD_VERSION_SUFFIX-$BUILD_BRANCH.$BUILD_COMMIT"
  export BUILD_VERSION=${BUILD_VERSION:-$_BUILD_VERSION}

  export DOCKER_BUILD_IMAGE_NAME=${DOCKER_BUILD_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
  export DOCKER_RELEASE_IMAGE_NAME=${DOCKER_RELEASE_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
  export DOCKERFILE=${DOCKERFILE:-"$CI_PROJECT_DIR/Dockerfile"}
  export DOCKER_CONTEXT=${DOCKER_CONTEXT:-"$CI_PROJECT_DIR"}
  export DOCKER_REGISTRY_USERNAME=${DOCKER_REGISTRY_USERNAME:-"gitlab-ci-token"}
  export DOCKER_REGISTRY_PASSWORD=${DOCKER_REGISTRY_PASSWORD:-"$CI_JOB_TOKEN"}
  export DOCKER_REGISTRY_URL=${DOCKER_REGISTRY_URL:-"$CI_REGISTRY"}
  export DOCKER_BUILD_IMAGE_NAME=${DOCKER_BUILD_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
  export DOCKER_RELEASE_IMAGE_NAME=${DOCKER_RELEASE_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
  export DOCKER_HOST=${DOCKER_HOST:-"unix:///var/run/docker.sock"}
  export DOCKER_TLS_CERTDIR=${DOCKER_TLS_CERT_DIR:-""}
  export DOCKER_BUILDKIT=${DOCKER_BUILDKIT:-1}
  export DOCKER_MULTIPLATFORM=${DOCKER_MULTIPLATFORM:-0}
  export DOCKER_MULTIPLATFORM_PLATFORMS=${DOCKER_MULTIPLATFORM_PLATFORMS:-"linux/amd64,linux/arm64"}

  export PACKAGE_NAME=${PACKAGE_NAME:-"${CI_PROJECT_NAME}"}
  export PACKAGE_TYPE=${PACKAGE_TYPE:-"generic"}
  export PACKAGE_REGISTRY_PROJECT_ID=${PACKAGE_REGISTRY_PROJECT_ID:-"${CI_PROJECT_ID}"}
  export PACKAGE_REGISTRY_TOKEN=${PACKAGE_REGISTRY_TOKEN:-"${CI_JOB_TOKEN}"}
  export PACKAGE_REGISTRY_PACKAGE_TYPE=${PACKAGE_REGISTRY_PACKAGE_TYPE:-"${PACKAGE_TYPE}"}

  export PROJECT_NAME=${PROJECT_NAME:-"${CI_PROJECT_NAME}"}
  export PROJECT_NAMESPACE=${PROJECT_NAMESPACE:-"${CI_PROJECT_NAMESPACE}"}
  export PROJECT_PATH_SLUG=${PROJECT_PATH_SLUG:-"${CI_PROJECT_PATH_SLUG}"}
  export PROJECT_PATH=${PROJECT_PATH:-"${CI_PROJECT_PATH}"}
  export PROJECT_ROOT_NAMESPACE=${PROJECT_ROOT_NAMESPACE:-"${CI_PROJECT_ROOT_NAMESPACE}"}
  export PROJECT_TITLE=${PROJECT_TITLE:-"${CI_PROJECT_TITLE}"}

  export S3_URL=${S3_URL:-""}
  export S3_USERNAME=${S3_USERNAME:-""}
  export S3_PASSWORD=${S3_PASSWORD:-""}
  export S3_BUCKET=${S3_BUCKET:-""}
  
  export UPDATE_VERSION_FILES=${UPDATE_VERSION_FILES:-1}
  export APPRISE_URL=${APPRISE_URL:-""}

  # Load shipment
  if [ -f "shipment.env" ]; then
    . shipment.env
  fi
}

# semantic-release saves the next version to RELEASE_VERSION
# which we will rename to NEXT_VERSION to not have conflicting artifacts in different
# stages our our pipelines
generate-next-version() {
  semantic-release --generate-notes false --ci false --branches $BUILD_BRANCH --dry-run
  touch RELEASE_VERSION
  mv RELEASE_VERSION NEXT_VERSION
  export NEXT_VERSION=$(cat NEXT_VERSION)

  # Commits of type chore or refactor won't yield a NEXT_VERSION
  # thus we're setting this to $LAST_VERSION to not break builds
  # due to a missing version
  if [ "$NEXT_VERSION" == "" ];then
    export NEXT_VERSION=$LATEST_VERSION
  fi

  unset BUILD_VERSION
  init-shipment
}

release-code() {
  # Check if a .releaserc.yml exists
  if [ ! -f "$PWD/.releaserc.yml" ]; then
    # Copy .releaserc.yml from shipmate
    cp /shipmate/.releaserc.yml $PWD/.releaserc.yml
  fi
  semantic-release
}

# shipmate export [--file shipment.env]
save-shipment() {
  echo "Saving shipment to shipment.env"
  #expose-shipment 
  print-versions > "shipment.env"
}

# not possible in other languages than bash
expose-shipment() {
  export LATEST_VERSION=$(get-latest-version)
  export BUILD_VERSION=$(get-build-version)
  export NEXT_VERSION=$(get-next-version)
  export RELEASE_VERSION=$(get-release-version)

  export CI_BRANCH=$(get-ci-branch)
  export BUILD_BRANCH=$(get-build-branch)
  export BUILD_COMMIT=$(get-build-commit)
  export BUILD_VERSION_SUFFIX=$(get-build-version-suffix)
  export DEFAULT_VERSION=$(get-default-version)
  export LATEST_TAG=$(get-latest-tag)

  export DOCKERFILE=$(get-dockerfile)
  export DOCKER_CONTEXT=$(get-docker-context)
  export DOCKER_REGISTRY_USERNAME=$(get-docker-registry-username)
  export DOCKER_REGISTRY_PASSWORD=$(get-docker-registry-password)
  export DOCKER_REGISTRY_URL=$(get-docker-registry-url)
  export DOCKER_HOST=$(get-docker-host)
  export DOCKER_TLS_CERTDIR=$(get-docker-tls-cert-dir)
  export DOCKER_BUILDKIT=$(get-docker-buildkit)
  export DOCKER_MULTIPLATFORM=$(get-docker-multiplatform)
  export DOCKER_BUILD_IMAGE_NAME=$(get-docker-build-image-name)
  export DOCKER_RELEASE_IMAGE_NAME=$(get-docker-release-image-name)

  export PACKAGE_NAME=$(get-package-name)
  export PACKAGE_TYPE=$(get-package-type)
  export PACKAGE_REGISTRY_TOKEN=$(get-package-registry-token)
  export PACKAGE_REGISTRY_PROJECT_ID=$(get-package-registry-project-id)
  export PACKAGE_REGISTRY_PACKAGE_TYPE=$(get-package-registry-package-type)

}

print-versions() {
  echo "DEFAULT_VERSION=$DEFAULT_VERSION"
  echo "LATEST_VERSION=$LATEST_VERSION"
  echo "BUILD_VERSION=$BUILD_VERSION"
  echo "NEXT_VERSION=$NEXT_VERSION"
  echo "RELEASE_VERSION=$RELEASE_VERSION"
}

print-shipment() {
  echo "DEFAULT_VERSION=$DEFAULT_VERSION"
  echo "LATEST_VERSION=$LATEST_VERSION"
  echo "BUILD_VERSION=$BUILD_VERSION"
  echo "NEXT_VERSION=$NEXT_VERSION"
  echo "RELEASE_VERSION=$RELEASE_VERSION"

  echo "CI_BRANCH=$CI_BRANCH"
  echo "BUILD_BRANCH=$BUILD_BRANCH"
  echo "BUILD_COMMIT=$BUILD_COMMIT"
  echo "BUILD_VERSION_SUFFIX=$BUILD_VERSION_SUFFIX"
  echo "LATEST_TAG=$LATEST_TAG"

  echo "DOCKERFILE=$DOCKERFILE"
  echo "DOCKER_CONTEXT=$DOCKER_CONTEXT"
  echo "DOCKER_REGISTRY_URL=$DOCKER_REGISTRY_URL"
  echo "DOCKER_HOST=$DOCKER_HOST"
  echo "DOCKER_TLS_CERTDIR=$DOCKER_TLS_CERTDIR"
  echo "DOCKER_BUILDKIT=$DOCKER_BUILDKIT"
  echo "DOCKER_BUILD_IMAGE_NAME=$DOCKER_BUILD_IMAGE_NAME"
  echo "DOCKER_RELEASE_IMAGE_NAME=$DOCKER_RELEASE_IMAGE_NAME"

  echo "PACKAGE_NAME=$PACKAGE_NAME"
  echo "PACKAGE_TYPE=$PACKAGE_TYPE"
  echo "PACKAGE_REGISTRY_PROJECT_ID=$PACKAGE_REGISTRY_PROJECT_ID"
  echo "PACKAGE_REGISTRY_PACKAGE_TYPE=$PACKAGE_REGISTRY_PACKAGE_TYPE"
}

# shipmate load
# no possible without bash
load-shipment() {
  if [ ! -f "shipment.env" ]; then
    echo "shipment.env not found"
    sleep 5
    return 1
  fi
  echo "Loading shipment from shipment.env"
  source "shipment.env"
  print-shipment
}

get-docker-build-image-name() {
  echo $DOCKER_BUILD_IMAGE_NAME
}

get-docker-release-image-name() {
  echo $DOCKER_RELEASE_IMAGE_NAME
}

get-docker-release-image-name() {
  echo $DOCKER_RELEASE_IMAGE_NAME
}

# shipmate validate $JOB
check-conditions() {
  JOB=$1
  if [ -z $JOB ];
  then
    echo "Please specify a job to check for: release.code, release.docker.*"
    sleep 5
    return 1
  fi

  echo "Checking conditions for job $JOB"

  if [[ "$JOB" == *"release.code"* ]]; then
    [[ -z $BUILD_VERSION ]] && echo "BUILD_VERSION not set" && exit 0
    [[ -z $NEXT_VERSION ]] && echo "NEXT_VERSION not set" && exit 0
    [[ -z $PRE_RELEASE_VERSION ]] && echo "PRE_RELEASE_VERSION not set" && exit 0
    [[ ! -f "CHANGELOG.md" ]] && touch CHANGELOG.md
  fi

  if [[ "$JOB" == *"release.package"* ]]; then
    if [[ -z $BUILD_VERSION ]]; then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ -z $NEXT_VERSION ]]; then
      echo "NEXT_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ "$LATEST_VERSION" == "$NEXT_VERSION" ]]; then
      echo "NEXT_VERSION equals LATEST_VERSION"
      sleep 5
      exit 0
    fi
  fi

  if [[ "$JOB" == *"build.docker"* ]]; then
    if [[ -z $BUILD_VERSION ]]; then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 1
    fi
  fi

  if [[ "$JOB" == *"release.docker"* ]]; then
    if [[ -z $BUILD_VERSION ]];  then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ -z $NEXT_VERSION ]]; then
      echo "NEXT_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ "$LATEST_VERSION" == "$NEXT_VERSION" ]]; then
      echo "NEXT_VERSION equals LATEST_VERSION"
      sleep 5
      exit 0
    fi
  fi
}

# Notify with Apprise
# @param TITLE is the message title
# @param BODY is the message body
notify() {
  if ! [ -x "$(command -v apprise)" ]; then
    echo 'Error: apprise is not installed.' >&2
    exit 1
  fi

  if [ "$1" == "" ];
  then
    echo "Please specify a title for the notification"
    sleep 5
    return 1
  else
    TITLE=$1
  fi

  if [ "$2" == "" ];
  then
    echo "Please specify a body for the notification"
    sleep 5
    return 1
  else
    BODY=$2
  fi

  if [ -z $APPRISE_URL ]; then
    echo "APPRISE_URL not set"
    sleep 5
    return 1
  fi

  apprise -t "$TITLE" -b "$BODY" $APPRISE_URL
}

notify-docker() {
  VERSION=$BUILD_VERSION

  if [ -n $1 ]; then
    VERSION=$1
  fi

  cat >/tmp/notify-docker-$CI_COMMIT_SHORT_SHA <<EOL
- Image: \`$DOCKER_BUILD_IMAGE_NAME\`
- Version: \`$VERSION\`
- Pull: \`docker pull $DOCKER_BUILD_IMAGE_NAME:$VERSION\` 
- Run: \`docker run $DOCKER_BUILD_IMAGE_NAME:$VERSION\` 
- Commit: $CI_PROJECT_URL/-/commit/$BUILD_COMMIT
EOL
  BODY=$(cat /tmp/notify-docker-$CI_COMMIT_SHORT_SHA)

  apprise -t "New Docker image for $PROJECT_PATH" -b "$BODY" $APPRISE_URL
}

# shipmate gitlab mr comment -m "COMMENT" $MR_ID
# shipmate notify gitlab mr --template $JOB
comment-mr() {
  if [ -z $1 ];
  then
    echo "Please specify a job to check for: build.docker"
    sleep 5
    return 1
  fi

  if [[ "$1" == *"build.docker"* ]]; then
    if [[ -n "$CI_MERGE_REQUEST_IID" ]]; then
      cat >/tmp/mrcomment-$CI_MERGE_REQUEST_IID <<EOL
## :whale: new image released

* Image: \`$DOCKER_BUILD_IMAGE_NAME\`
* Version: \`$BUILD_VERSION\`
* Pull: \`docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION\` 
* Run: \`docker run $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION\` 
* Commit: $CI_PROJECT_URL/-/commit/$BUILD_COMMIT
EOL
      #NOTE="## :whale: new image released\n\nVersion: \`$BUILD_VERSION\`\nImage URL: \`$DOCKER_IMAGE_NAME:$BUILD_VERSION\`"
      [[ -z $GITLAB_TOKEN ]] && echo "$GL_TOKEN" | glab auth login --stdin
      echo "Updating Merge Request $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/!${CI_MERGE_REQUEST_IID}: ${CI_MERGE_REQUEST_TITLE}"
      echo "Check it here: $(glab --repo "$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME" mr note -m "$(cat /tmp/mrcomment-$CI_MERGE_REQUEST_IID)" $CI_MERGE_REQUEST_IID)"
      rm /tmp/mrcomment-$CI_MERGE_REQUEST_IID
    fi
  fi

  if [[ "$1" == *"build.package"* ]]; then
    if [[ -n "$CI_MERGE_REQUEST_IID" ]]; then
      cat >/tmp/mrcomment-$CI_MERGE_REQUEST_IID <<EOL
## :package: new package released

* Package: \`$PACKAGE_NAME\`
* Version: \`$BUILD_VERSION\`
* Download: \`TBD\` 
EOL
      [[ -z $GITLAB_TOKEN ]] && echo "$GL_TOKEN" | glab auth login --stdin
      echo "Updating Merge Request $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/!${CI_MERGE_REQUEST_IID}: ${CI_MERGE_REQUEST_TITLE}"
      echo "Check it here: $(glab --repo "$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME" mr note -m "$(cat /tmp/mrcomment-$CI_MERGE_REQUEST_IID)" $CI_MERGE_REQUEST_IID)"
      rm /tmp/mrcomment-$CI_MERGE_REQUEST_IID
    fi
  fi
  
}

# Backwards compatibility
update-version() {
  update-version-files
}

update-version-files() {
  if [ "$UPDATE_VERSION_FILES" == "1" ]; then
    if [ -z $NEXT_VERSION ];
    then
      echo "NEXT_VERSION not set - not updating anything"
      exit 0
    fi

    export RELEASE_VERSION=${RELEASE_VERSION:-$NEXT_VERSION}
    export PROJECT_NAME=${CI_PROJECT_NAME:-$(basename $PWD)}

    if [ -d charts/docs ];
    then
      yq e -i '.version = strenv(RELEASE_VERSION)' charts/docs/Chart.yaml
      yq e -i '.appVersion = strenv(RELEASE_VERSION)' charts/docs/Chart.yaml
      yq e -i '.image.tag = strenv(RELEASE_VERSION)' charts/docs/values.yaml
    fi

    if [ -d charts/$PROJECT_NAME ];
    then
      yq e -i '.version = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/Chart.yaml
      yq e -i '.appVersion = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/Chart.yaml
      yq e -i '.image.tag = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/values.yaml
    fi
  else
    echo "UPDATE_VERSION_FILES is $UPDATE_VERSION_FILES - not updating anything"
  fi
}

build-docker() {
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_RELEASE_IMAGE_NAME:latest || true
  
  if [ "$DOCKER_MULTIPLATFORM" == "1" ]; then
    echo "Running in multiplatform mode"
    SHIPMATE_BUILDER_EXISTS=1
    docker buildx inspect shipmate || SHIPMATE_BUILDER_EXISTS=0

    if [ "SHIPMATE_BUILDER_EXISTS" == "1" ]; then
      echo "Buildx builder exists, using it"
      docker buildx use shipmate
    else
      echo "Buildx builder does not exist, creating it"
      docker run --privileged multiarch/qemu-user-static --reset -p yes
      docker buildx create --name shipmate --driver docker-container --use
      docker buildx inspect --bootstrap
    fi

    # Run a build for each platform/architecture
    IMAGES=""
    OLDIFS=$IFS
    IFS=',' read -ra PLATFORMS <<< "$DOCKER_MULTIPLATFORM_PLATFORMS"
    for PLATFORM in "${PLATFORMS[@]}"; do
      IFS='/' read -ra PLATFORM_PARTS <<< "$PLATFORM"
      echo "Building image for ${PLATFORM}"
      docker buildx build -f $DOCKERFILE --cache-from $DOCKER_RELEASE_IMAGE_NAME:latest --push --platform $PLATFORM --build-arg APP_BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg APP_BUILD_VERSION=$BUILD_VERSION --build-arg APP_AUTHOR=$CI_PROJECT_NAMESPACE --build-arg APP_NAME=$CI_PROJECT_NAME -t $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION-${PLATFORM_PARTS[0]}-${PLATFORM_PARTS[1]} $DOCKER_CONTEXT

      IMAGES="$IMAGES $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION-${PLATFORM_PARTS[0]}-${PLATFORM_PARTS[1]}"
    done
    IFS=$OLDIFS

    # docker push --all-tags -q $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION

    echo "Creating Manifest"
    docker manifest create $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $IMAGES

    echo "Pushing Manifest"
    docker manifest push $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION
    
    # docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    # docker buildx create --name shipmate --driver docker-container --use
    # docker buildx inspect --bootstrap
  else
    echo "Building image"
    docker build -f $DOCKERFILE --cache-from $DOCKER_RELEASE_IMAGE_NAME:latest --build-arg APP_BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg APP_BUILD_VERSION=$BUILD_VERSION --build-arg APP_AUTHOR=$CI_PROJECT_NAMESPACE --build-arg APP_NAME=$CI_PROJECT_NAME -t $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_CONTEXT
    echo "Pushing image"
    docker push $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION && echo "Released image $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION"
  fi
  #notify "New image available for $PROJECT_PATH" "\`docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION\`"
}

release-docker() {
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION

  if [ "$DOCKER_MULTIPLATFORM" == "1" ]; then
    echo "Running in multiplatform mode"
    IMAGES=""
    OLDIFS=$IFS
    IFS=',' read -ra PLATFORMS <<< "$DOCKER_MULTIPLATFORM_PLATFORMS"
    for PLATFORM in "${PLATFORMS[@]}"; do
      IFS='/' read -ra PLATFORM_PARTS <<< "$PLATFORM"
      IMAGES="$IMAGES $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION-${PLATFORM_PARTS[0]}-${PLATFORM_PARTS[1]}"
    done
    IFS=$OLDIFS

    docker manifest create $DOCKER_BUILD_IMAGE_NAME:$NEXT_VERSION $IMAGES
    docker manifest create $DOCKER_BUILD_IMAGE_NAME:latest $IMAGES

    docker manifest push $DOCKER_BUILD_IMAGE_NAME:$NEXT_VERSION
    docker manifest push $DOCKER_BUILD_IMAGE_NAME:latest

  else
    docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:$NEXT_VERSION
    docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:latest
    docker push $DOCKER_RELEASE_IMAGE_NAME:$NEXT_VERSION && echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:$NEXT_VERSION"
    docker push $DOCKER_RELEASE_IMAGE_NAME:latest && echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:latest"
  fi
}


release.docker() {
  DOCKER_BUILD_IMAGE_NAME=$1
  DOCKER_RELEASE_IMAGE_NAME=$2
  BUILD_VERSION=$3
  RELEASE_VERSION=$4
  DOCKER_REGISTRY_URL=$5
  DOCKER_REGISTRY_USERNAME=$6
  DOCKER_REGISTRY_PASSWORD=$7
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:latest
  docker push $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION
  echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION"
  docker push $DOCKER_RELEASE_IMAGE_NAME:latest
  echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:latest"
}

build-package() {
  # Check if on default branch
  # if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
  #   # Set BUILD_VERSION to NEXT_VERSION
  #   # If we're building on main, we want to have the final version number
  #   # baked into images and artifacts
  #   export BUILD_VERSION=$NEXT_VERSION
  # fi
  BIN_FILE_NAME_PREFIX=$PACKAGE_NAME
  PROJECT_DIR=${PROJECT_DIR:-$CI_PROJECT_DIR}
  OUTPUT_DIR=packages/$PACKAGE_TYPE

  cd $PROJECT_DIR

  PLATFORMS="linux/amd64 linux/arm64 darwin/amd64 darwin/arm64"
  for PLATFORM in $PLATFORMS; do
    GOOS=${PLATFORM%/*}
    GOARCH=${PLATFORM#*/}
    FILEPATH="$PROJECT_DIR/$OUTPUT_DIR/${GOOS}-${GOARCH}"
    #echo $FILEPATH
    mkdir -p $FILEPATH
    BIN_FILE_NAME="$FILEPATH/${BIN_FILE_NAME_PREFIX}-${GOOS}-${GOARCH}"
    echo $BIN_FILE_NAME
    if [[ "${GOOS}" == "windows" ]]; then BIN_FILE_NAME="${BIN_FILE_NAME}.exe"; fi
    
    PACKAGE_BUILD_VERSION=$BUILD_VERSION
    # If we're on "main" (default branch), we're building the package with $NEXT_VERSION to have better version output
    if [[ "${CI_BRANCH}" == "${CI_DEFAULT_BRANCH}" ]]; then PACKAGE_BUILD_VERSION=$NEXT_VERSION; fi

    CMD="GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags=\"-X 'cloudstack/cmd.version=${PACKAGE_BUILD_VERSION}'\" -o ${BIN_FILE_NAME}"
    #echo $CMD
    echo "${CMD}"
    eval $CMD || FAILURES="${FAILURES} ${PLATFORM}"

    if [ "$FAILURES" != "" ]; then
      echo $FAILURES
      exit 1
    fi
  done
}

# shipmate build package
# shipmate release package --package-name NAME --package-type generic --package-version VERSION --package-registry-project-id ID --package-registry-package-type generic --package-registry-token TOKEN
release-package() {
  PACKAGE_REGISTRY_PACKAGE_TYPE=""
  PACKAGE_REGISTRY_BASE="${CI_API_V4_URL}/projects/${PACKAGE_REGISTRY_PROJECT_ID}/packages"
  PACKAGE_REGISTRY_TOKEN_TYPE="JOB-TOKEN"
  PACKAGE_VERSION=$RELEASE_VERSION

  if [ "$PACKAGE_TYPE" == "go" ]; then
    PACKAGE_REGISTRY_PACKAGE_TYPE="generic"
  fi
  echo "PACKAGE_REGISTRY_PACKAGE_TYPE=$PACKAGE_REGISTRY_PACKAGE_TYPE"
  if [ "$PACKAGE_REGISTRY_PROJECT_ID" != "$CI_PROJECT_ID" ]; then
    PACKAGE_REGISTRY_TOKEN_TYPE="PRIVATE-TOKEN"
  fi
  echo "PACKAGE_REGISTRY_TOKEN_TYPE=$PACKAGE_REGISTRY_TOKEN_TYPE"

  PACKAGE_REGISTRY_URL="$PACKAGE_REGISTRY_BASE/${PACKAGE_REGISTRY_PACKAGE_TYPE:-$PACKAGE_TYPE}/${PACKAGE_NAME}/${PACKAGE_VERSION}"
  for file in `find packages/$PACKAGE_TYPE -type f`; do
    echo "Uploading $file to ${PACKAGE_REGISTRY_URL}"
    curl --header "$PACKAGE_REGISTRY_TOKEN_TYPE: ${PACKAGE_REGISTRY_TOKEN}" --upload-file $file ${PACKAGE_REGISTRY_URL}/$(basename $file)
  done
}

release-package-s3() {
  PACKAGE_VERSION=$RELEASE_VERSION
  PROJECT_DIR=${PROJECT_DIR:-$CI_PROJECT_DIR}

  cd $PROJECT_DIR

  # Prepare s3
  mc alias set s3 $S3_URL $S3_USER $S3_PASSWORD --api S3v4
  for file in `find packages/$PACKAGE_TYPE -type f`; do
    if [ "$PACKAGE_VERSION" == "latest" ]; then
      echo "Deleting version 'latest' for $file from ${S3_URL}"
      mc ls s3/$S3_BUCKET/$PACKAGE_NAME/latest/$(basename $file) && mc rm s3/$S3_BUCKET/$PACKAGE_NAME/latest/$(basename $file) || true
    fi

    echo "Uploading version '$PACKAGE_VERSION' for $file to ${S3_URL}"
    # Destination structure
    # s3/$BUCKET/$PACKAGE_NAME/binary.sh
    mc cp $file s3/$S3_BUCKET/$PACKAGE_NAME/$PACKAGE_VERSION/$(basename $file)
  done

  if [ "$PACKAGE_VERSION" != "latest" ]; then
    # Write RELEASE_VERSION to `stable`
    echo $PACKAGE_VERSION > stable

    # Copy `stable` to the package repo root
    mc ls s3/$S3_BUCKET/$PACKAGE_NAME/stable && mc mv stable s3/$S3_BUCKET/$PACKAGE_NAME/stable || true
  fi
  
}

init-shipment