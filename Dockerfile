FROM registry.gitlab.com/ayedocloudsolutions/adf:1.10.0-rc-main.89f7c283

RUN mkdir -p /shipmate

WORKDIR /shipmate

COPY shipmate.sh shipmate.sh
COPY .releaserc.yml .releaserc.yml
COPY docker-entrypoint.sh docker-entrypoint.sh
COPY update_values.sh update_values.sh

RUN chmod +x /shipmate/docker-entrypoint.sh
RUN chmod +x /shipmate/update_values.sh

ENTRYPOINT ["/shipmate/docker-entrypoint.sh"]

CMD ["bash"]

