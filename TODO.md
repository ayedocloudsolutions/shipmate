# TODO

## Concepts

`shipmate` deals with so called shipments. Shipmate helps with the workflow around software releases ("shipping").

## Shipment

```yaml
name: shipmate
namespace: ayedocloudsolutions
description: A tool to tool tools
version:
  last: 0.0.1
  next: 0.1.0
  rc: 0.1.0-rc.feature-add-graphql-asdasd23
  pattern: "{{ .version.next }}-rc.{{ .git.branch | trunc 15 }}-{{ .git.commit }}"
changelog: "asdasd"
git:
  repository: https://gitlab.com/ayedocloudsolutions/shipmate
  commit:
    author:
      name: Fabian Peter
      email: fp@ayedo.de
    sha: asdasdasdasdasdasdasd123
    id: asdasd23
    title: "fix: stuff"
    semver:
      type: patch
  commits:
    - sha: asdasdasdasdasdasdasd123
      id: asdasd23
      title: "fix: stuff"
      body: "..."
      footer: "..."
      author:
        name: Fabian Peter
        email: fp@ayedo.de
      semver:
        type: patch
  branch: main
  tag: 0.0.1
docker:
  registry: registry.gitlab.com
  image: ayedocloudsolutions/shipmate
helm:
  repository: ...
gitlab:
  mr:
    title:
    description:
    id:
    url:
    iid:
```

## Workflow

- open path
- check if git repo
  - exit if not
- get branch
- get commit
- get last tag
- get commits since last tag
- analyze commits
- weed out non conventional commits
- compute next version
- set release candidate version
- compute changelog

## Assertions

- git tag == version
- version == semantic version v2
- initial version == 0.0.1
- --set manipulates the shipment
- keywords:
  - patch:
    - fix
  - minor:
    - feat
  - major:
    - body "BREAKING CHANGE"
  - skip:
    - docs
    - chore
    - perf
    - test
    - ci
    - build
    - revert
    - refactor
    - style

## Configuration precedence

1. Command line
2. Environment variable
3. Configuration file
4. Defaults

## Commands

- global flags
  - --set
- shipmate get
  - --value "author.name" [multi possible]
    - returns the respective values
  - --format [default: yaml; json, pretty]
- shipmate branch
  - prints the current branch
- shipmate version
  - prints the release candidate version
  - --last
    - retrieves latest version from git tags
    - same as "shipmate tag"
  - --next
    - calculates next version from conventional commits since latest-version
  - --rc
    - release candidate version
- shipmate validate
- shipmate changelog [> CHANGELOG.md]
  - prints the changelog
  - --format [default: pretty; json, yaml]
- shipmate print
  - prints the shipment
  - same as "--format pretty"
- shipmate dump
  - --format [default: yaml; json, pretty]

## Styling

```yaml
- type: "feat"
  section: ":sparkles: Features"
  hidden: false
- type: "feature"
  section: ":sparkles: Features"
  hidden: false
- type: "fix"
  section: ":bug: Fixes"
  hidden: false
- type: "docs"
  section: ":memo: Documentation"
  hidden: false
- type: "style"
  section: ":barber: Style"
  hidden: false
- type: "refactor"
  section: ":zap: Refactoring"
  hidden: false
- type: "revert"
  section: ":zap: Reverts"
  hidden: false
- type: "perf"
  section: ":fast_forward: Performance Improvements"
  hidden: false
- type: "test"
  section: ":white_check_mark: Tests"
  hidden: false
- type: "ci"
  section: ":repeat: CI"
  hidden: false
- type: "chore"
  section: ":repeat: Chores"
  hidden: false
- type: "build"
  section: ":repeat: Build System"
  hidden: false
```