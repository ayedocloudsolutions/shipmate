module shipmate

go 1.16

require (
	github.com/go-git/go-git v4.7.0+incompatible // indirect
	github.com/go-git/go-git/v5 v5.4.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	github.com/wfscheper/convcom v0.0.0-20200418012201-7aa0e60ba66c // indirect
)
